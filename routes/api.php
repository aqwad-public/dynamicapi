<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

$namespace = '\Akwad\dynamicapi';

Route::group([
    'namespace' => $namespace,
    'middleware' => [\Akwad\dynamicapi\Middlewares\CheckModlePermissions::class],
], function () {

    Route::post('api/{model}', ['uses' => 'QueryParser@parse', 'as' => 'parse']);
});

<?php

namespace Akwad\dynamicapi\Facades;

use Illuminate\Support\Facades\Facade;

class Runner extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'apicoderunner';
    }
}

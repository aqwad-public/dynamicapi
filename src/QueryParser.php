<?php

namespace Akwad\dynamicapi;

use Akwad\dynamicapi\Exceptions\FunctionDoesnotExist;
use Akwad\dynamicapi\Exceptions\InsertValueNotArray;
use Akwad\dynamicapi\Exceptions\ModelDoesntExist;
use Akwad\dynamicapi\Exceptions\QueryNotFound;
use Akwad\dynamicapi\Exceptions\SelectAndInsertNotFound;
use Akwad\dynamicapi\Exceptions\SelectValueNotArray;
use Akwad\dynamicapi\Exceptions\ValueDoesntExist;
use Akwad\dynamicapi\Facades\Runner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use TheSeer\Tokenizer\Exception;
use TCG\Voyager\Facades\Voyager;
use Akwad\dynamicapi\Translator;

class QueryParser extends Controller
{
    protected $tableColumns = [];
    protected $tableName = "";
    protected $model = [];
    protected $mode = '';
    protected $beforeRunnables = [];
    protected $afterRunnables = [];

    public function parse(Request $request, $model)
    {

        //create the model
        try {
            $this->model = app('\App\\' . $model);
        } catch (Exception $e) {
            throw new ModelDoesntExist();
        }

        $this->tableColumns = $this->listTableColumnNames($this->model->getTable());

        /**
         * we must check for the query variable first
         * $query contains Where and With vars
         **/

        if (!$request->filled('query')) {
            throw new QueryNotFound();
        }

        $query = $request->input('query');

        // check for where existance
        if (isset($query['where'])) {
            $this->model = $this->whereParse($this->model, $query['where']);
        }
        // check for relationship constrains
        if (isset($query['has'])) {
            $this->model = $this->has($this->model, $query['has']);
        }

        // check for order by
        if (isset($query['orderBy'])) {
            $this->model->orderBy($query['orderBy'][0], $query['orderBy'][1]);
        }

        // check for limit by
        if (isset($query['limit'])) {
            $this->model->limit($query['limit']);
        }

        // check for limit by
        if (isset($query['offset'])) {
            $this->model->offset($query['offset']);
        }

        // check for the select or insert
        // runnder start
        if ($request->filled('runnables')) {

            foreach ($request->input('runnables') as $runnable) {
                if (strpos($runnable, ':before') > -1) {
                    $this->beforeRunnables[] = explode(':', $runnable)[0];
                } else {
                    $this->afterRunnables[] = $runnable;
                }
            }
        }

        if ($request->filled('select')) {
            $this->model = $this->selectParse($this->model, $request->input('select'));
            $this->model = Runner::runBefore($this->beforeRunnables, $this->model);
        } else if ($request->filled('insert')) {
            $this->model = $this->insert($this->model, $request->input('insert'), function ($model) {
                return Runner::runBefore($this->beforeRunnables, $model);
            });
        } else {
            throw new SelectAndInsertNotFound();
        }

        if ($request->filled('debug') && $request->debug === "query" && App::environment(['development', 'local'])) {
            dd(str_replace_array('?', $this->model->getBindings(), $this->model->toSql()));
        }


        $result = [];
        if (Voyager::translatable('App\\' . $model)) { // if model is translatable we replace the original values with the reqeusted locale's 

            $results = $this->model->get()->map(function ($item, $key) use ($request) {
                return (new Translator($item))->translate($request->lang, 'en');
            });
        } else {
            $results = $this->model->get();
        }
        // runnder start
        if (count($this->afterRunnables)) {

            $runnerResults = Runner::run($this->afterRunnables, $results);

            $results = array_merge($results->toArray(), $runnerResults);
        }

        // runner end

        return response()->json([
            'errorsNo' => 0,
            'errorMsgs' => "",
            'data' => $results, // return $results instead
        ], 200);
    }
    private function insert($model, $insertValues, $runBeforeInsert)
    {

        // insert should be an array
        if (!is_array($insertValues)) {
            throw new InsertValueNotArray();
        }

        $results = [];

        // if this is a builder get the results else, assing the model to the results array
        if (get_class($model) === "Illuminate\Database\Eloquent\Builder") {

            if (!isset($insertValues[0])) {

                throw new InsertValueNotArray();
            }

            $results = $model->get()->all();

            foreach ($results as $result) {
                $result->fill($insertValues[0]); // takes car of inserting only allowed values
                $result = $runBeforeInsert($result);
                $result->save();
            }
            return $model;
        }

        if (!is_array(Arr::first($insertValues, function () {
            return true;
        }))) {

            $model->fill($insertValues); // takes car of inserting only allowed values
            $result = $runBeforeInsert($result);
            $model->save();

            return $model::where($model->getKeyName(), $model->id);
        } else {

            $insertedIds = [];

            foreach ($insertValues as $insertValue) {
                $tempModel = app(get_class($model));
                $tempModel->fill($insertValue); // takes car of inserting only allowed values
                $tempModel = $runBeforeInsert($tempModel);

                $tempModel->save();
                $insertedIds[] = $tempModel->id;
            }

            return $model::whereIn('id', $insertedIds);
        }

        return $model;
    }

    private function parseWith($model, $select)
    {

        $withArgs = [];
        $with = [];

        foreach ($select as $key => $column) { // iterate over select values

            if (strpos($column, '.') === false) {
                continue;
            }

            $parts = explode('.', $column);
            $withArgs[$parts[0]][] = $parts[1];
        }

        foreach ($withArgs as $key => $withColumns) {
            $with[$key] = function ($q) use ($withColumns) {
                $q->select($withColumns);
            };
        }

        return $model->with($with);
    }

    /**
     * A recusive function that discovers json turning it into the WHERE clouse part
     */
    private function whereParse($model, $query, $type = 'and')
    {

        if (!isset($query['value'])) {

            throw new ValueDoesntExist();
        }

        // if just string table must have it as column
        if (!in_array($query['value'][0], $this->tableColumns)) {
            // TODO:: add an exception about a value that doesn't exist in our model
        }

        // type doesn't exist so we have the simplest case
        if (!isset($query['type'])) {
            return $type == "and" ?
                $model->where($query['value'][0], $query['value'][1], $query['value'][2]) : $model->orWhere($query['value'][0], $query['value'][1], $query['value'][2]);
        }

        if ($type == 'and') {
            return $model->where(function ($inner) use ($query) {
                foreach ($query['value'] as $value) {
                    $this->whereParse($inner, $value, $query['type']);
                }
            });
        } else {
            return $model->orWhere(function ($inner) use ($query) {
                foreach ($query['value'] as $value) {
                    $this->whereParse($inner, $value, $query['type']);
                }
            });
        }
    }

    private function has($model, $query)
    {

        // should be an array
        if (!is_array($query)) {
            //TODO:: array exception.
        }

        foreach ($query as $relationship) {

            $constrains = function ($q) use ($relationship) {
                // check for where existance
                if (!isset($relationship['where'])) {
                    return;
                }
                $this->whereParse($q, $relationship['where']);
            };

            if ($relationship['type'] === 'and') {
                $model = $model->whereHas($relationship['name'], $constrains);
            } else {
                $model = $model->orWhereHas($relationship['name'], $constrains);
            }
        }

        return $model;
    }

    private function selectParse($model, $selectValues)
    {

        // select should be an array
        if (!is_array($selectValues)) {
            throw new SelectValueNotArray();
        }
        // select all the related model first
        $model = $this->parseWith($model, $selectValues);

        // remove all values conataining (.) before feeding the values to select
        foreach ($selectValues as $key => $value) {
            if (strpos($value, '.') === true) {
                unset($selectValues[$key]);
            }
        }

        // loop over the array
        foreach ($selectValues as $value) {
            //     see if the value is a function we can call
            $pos = strpos($value, '(');
            if ($pos) {
                $this->call($value, $model);
                continue;
            }

            // if just string table must have it as column
            if (!in_array($value, $this->tableColumns)) {
                continue;
            }

            $model = $model->addSelect($value);
        }

        return $model;
    }

    private function call($function, $model)
    {
        $pos = strpos($function, '(');
        $funcName = substr($function, 0, $pos);

        if (!method_exists($this, $funcName)) {
            throw new FunctionDoesnotExist();
        }

        $args = explode(',', substr($function, $pos + 1, -1));
        $this->$funcName($model, $args);
    }

    public static function listTableColumnNames($tableName)
    {
        $schemaMangaer = DB::connection()->getDoctrineSchemaManager();

        $columnNames = [];

        foreach ($schemaMangaer->listTableColumns($tableName) as $column) {
            $columnNames[] = $column->getName();
        }

        return $columnNames;
    }
}

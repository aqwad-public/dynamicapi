<?php

namespace Akwad\dynamicapi\Middlewares;

use Closure;

class CheckModlePermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // check if we have an an api_token
        if ($request->has('api_token')) {
            if (!auth()->guard('api')->check()) {
                return response()->json([
                    'errorsNo' => 1,
                    'errorMsgs' => ['Token invalide or expired'],
                    'data' => [],
                ], 401);
            }

        } else if ($request->has('fcm_token')) {

        } else {
            return response()->json([
                'errorsNo' => 1,
                'errorMsgs' => ['api_token or fcm_token has to exit'],
                'data' => [],
            ], 200);
        }

        return $next($request);
    }
}

<?php

namespace Akwad\dynamicapi;

use Akwad\dynamicapi\ApiCodeRunner;
use Akwad\dynamicapi\Middlewares\CheckModlePermissions;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class SearchServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $loader = AliasLoader::getInstance();
        //  $loader->alias('Runner', Runner::class);

        $this->app->singleton('apicoderunner', function () {

            return new ApiCodeRunner();
        });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $router->middleware('akwad.dynamicapi', CheckModlePermissions::class);

        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
    }
}

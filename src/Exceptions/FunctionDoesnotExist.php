<?php

namespace Akwad\dynamicapi\Exceptions;

use Exception;

class FunctionDoesnotExist extends Exception
{
    public function render($request)
    {

        return response()->json([
            'errorsNo' => 1,
            'errorMsgs' => "The select function you typed doesn't exist",
            'data' => [],
        ], 404);
    }
}

<?php

namespace Akwad\dynamicapi\Exceptions;

use Exception;

class SelectValueNotArray extends Exception
{
    public function render($request)
    {

        return response()->json([
            'errorsNo' => 1,
            'errorMsgs' => "Select value must be an array",
            'data' => [],
        ], 400);
    }
}

<?php

namespace Akwad\dynamicapi\Exceptions;

use Exception;

class ModelDoesntExist extends Exception
{
    public function render($request)
    {

        return response()->json([
            'errorsNo' => 1,
            'errorMsgs' => "The model you typed doesn't exist",
            'data' => [],
        ], 404);
    }
}

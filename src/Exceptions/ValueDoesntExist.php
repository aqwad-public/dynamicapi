<?php

namespace Akwad\dynamicapi\Exceptions;

use Exception;

class ValueDoesntExist extends Exception
{
    public function render($request)
    {

        return response()->json([
            'errorsNo' => 1,
            'errorMsgs' => "Value attribute was mising",
            'data' => [],
        ], 400);
    }
}

<?php

namespace Akwad\dynamicapi\Exceptions;

use Exception;

class SelectAndInsertNotFound extends Exception
{
    public function render($request)
    {

        return response()->json([
            'errorsNo' => 1,
            'errorMsgs' => "You must supply either a select or an insert, none was found",
            'data' => [],
        ], 404);
    }
}

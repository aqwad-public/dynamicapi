<?php

namespace Akwad\dynamicapi\Exceptions;

use Exception;

class InsertValueNotArray extends Exception
{
    public function render($request)
    {

        return response()->json([
            'errorsNo' => 1,
            'errorMsgs' => "Insert value must be an array",
            'data' => [],
        ], 400);
    }
}

<?php

namespace Akwad\dynamicapi\Exceptions;

use Exception;

class QueryNotFound extends Exception
{
    public function render($request)
    {

        return response()->json([
            'errorsNo' => 1,
            'errorMsgs' => "Query paramter must allways exist",
            'data' => [],
        ], 400);
    }
}

<?php

namespace Akwad\dynamicapi;

use Illuminate\Routing\RouteDependencyResolverTrait;
use Illuminate\Support\Str;

class ApiCodeRunner
{

    use RouteDependencyResolverTrait;

    protected $runnables = [];
    protected $nameSpace = '\App\Http\Controllers\\';
    protected $results = [];



    /**
     * Runs a list of runnables before CRUD operation happening.
     *
     * @param  array $runnables
     * @param  mixed  $model
     * @return mixed
     */
    public function runBefore(array $runnables, $model)
    {

        foreach ($runnables as $runnable) {

            if (!isset($this->runnables[$runnable]['controller'])) {
                $actionParts = Str::parseCallback($this->runnables[$runnable]['action']);
                $this->runnables[$runnable]['controller'] = app()->make($this->nameSpace . ltrim($actionParts[0], '\\'));
            }

            $controller = $this->runnables[$runnable]['controller'];

            //TODO:: add laravel type hinte  depindancy injection
            // $parameters = $this->resolveClassMethodDependencies(
            //     $route->parametersWithoutNulls(), $controller, $method
            // );

            $model = $controller->{$actionParts[1]}($model);
        }

        return $model;
    }




    /**
     * Runs a list of runnables on the results returned from the database.
     *
     * @param  array $runnables
     * @param  mixed  $dbResults
     * @return mixed
     */
    public function run(array $runnables, $dbResults, $debug = false)
    {
        $results = [];


        foreach ($runnables as $runnable) {

            if (!isset($this->runnables[$runnable]['controller'])) {
                $actionParts = Str::parseCallback($this->runnables[$runnable]['action']);
                $this->runnables[$runnable]['controller'] = app()->make($this->nameSpace . ltrim($actionParts[0], '\\'));
            }

            $controller = $this->runnables[$runnable]['controller'];

            $controller->debug = $debug;

            //TODO:: add laravel type hinte  depindancy injection
            // $parameters = $this->resolveClassMethodDependencies(
            //     $route->parametersWithoutNulls(), $controller, $method
            // );

            $this->results[$runnable] = $controller->{$actionParts[1]}($dbResults);
        }

        return $this->results;
    }
    /**
     * Register anunnable to be run latter by the API.
     *
     * @param  string $runnableName
     * @param  string $runnable
     * @return void
     */
    public function registerRunnable($runnableName, $runnable)
    {
        // runnableName and runnable must be strings
        // runnable must have an "@"

        if (!is_string($runnableName) || !is_string($runnable) || !strpos($runnable, '@')) {
            throw new \Exception('Name and callable function must be strings with @', 0);
        }

        $this->runnables[$runnableName]['action'] = $runnable;
    }

    function namespace($nameSpace)
    {

        if (!is_string($nameSpace)) {
            throw new \Exception('Namespace must be a string', 0);
        }

        $this->nameSpace = $nameSpace;
    }
}

<?php

namespace Akwad\dynamicapi;


use Akwad\dynamicapi\Translator;

trait AkwadTranslatable
{


    public function translateRelation($key, $lang)
    {
        if (!$this->relationLoaded('translations')) {
            $this->load('translations');
        }
        if(!isset($this->relations[$key])){
            $this->load($key);
        }

        if($this->relations[$key] == null){
            return;
        }
        
        $this->relations[$key] = (new Translator($this->relations[$key]))->translate($lang, 'en');
    }
}
